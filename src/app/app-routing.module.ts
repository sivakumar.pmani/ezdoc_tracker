import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './components/layout/layout.component';
import { LandingComponent } from './components/landing/landing.component';


const routes: Routes = [
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  
  {
    path: '',
    component: LayoutComponent,
    children: [      
      {
        path: 'landing',
        component: LandingComponent,
      }
    ]
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

